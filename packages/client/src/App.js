import { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import Todo from './Components/Todo/Todo'
import TodoList from './Components/Todo/TodoList/TodoList'
import axios from './axios/axios'

function App() {

  const [isInTodo, setIsInTodo] = useState(false)
  const [todoList, setTodoList] = useState(null)

  const onTodoClickedHander = async (id, is_protected) => {
    try {
      console.log(id, is_protected)
      let todoList
      if (is_protected) {
        let password = prompt("Todo is protected. Please enter password.")
        todoList = await axios.getTodoList(id, password)
        if (todoList.success) {
          setTodoList({ todo_list: todoList.todo_list, is_protected })
          setIsInTodo(true)
        } else {
          alert(todoList.msg)
        }
      } else {
        todoList = await axios.getTodoList(id, null)
        if (todoList.success) {
          setTodoList({ todo_list: todoList.todo_list, is_protected })
          setIsInTodo(true)
        } else {
          alert(todoList.msg)
        }
      }
    } catch (e) {
      if (typeof e.response.data !== "undefined") {
        alert(e.response.data.msg)
      }
    }
  }

  const onBackClickedHandler = () => {
    setIsInTodo((privious) => {
      return !privious
    })
  }

  const reloadTodoListHandler = async (todo_id, password) => {
    let todoList = await axios.getTodoList(todo_id, password)
    setTodoList((privious) => {
      return { todo_list: todoList.todo_list, is_protected: privious.is_protected }
    })
  }

  return (
    <Container>
      {
        isInTodo ?
          <TodoList
            list={todoList.todo_list}
            protected={todoList.is_protected}
            onBackClicked={onBackClickedHandler}
            reloadTodoList={reloadTodoListHandler}
          />
          :
          <Todo
            onTodoClicked={onTodoClickedHander}
          />
      }
    </Container>
  )
}

export default App;
