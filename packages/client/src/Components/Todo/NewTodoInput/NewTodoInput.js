import { useState } from 'react'
import { ListGroup, Button, Card, Form } from 'react-bootstrap'
import axios from '../../../axios/axios'

const NewTodoInput = (props) => {
    const [title, setTitle] = useState('')
    const [password, setPassword] = useState('')

    const onTitleChangeHandler = (event) => {
        setTitle(event.target.value)
    }

    const onPasswordChangeHandler = (event) => {
        setPassword(event.target.value)
    }

    const onAddTodo = async () => {
        try {
            let tempPassword = null
            if (password != '') {
                tempPassword = password
            }
            await axios.addTodo(title, tempPassword)
            await props.reloadTodo()
            setTitle('')
            setPassword('')
        } catch (e) {
            if (typeof e.response.data !== "undefined") {
                alert(e.response.data.msg)
            }
        }
    }

    return (
        <Card body>
            <h5>New Todo</h5>
            <Form>
                <Form.Group>
                    <Form.Label>Todo Title</Form.Label>
                    <Form.Control type="text" value={title} onChange={onTitleChangeHandler} placeholder="Enter todo title" />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" value={password} onChange={onPasswordChangeHandler} placeholder="Password" />
                </Form.Group>
            </Form>
            <br />
            <Button variant="primary" onClick={onAddTodo}>+ Add</Button>
        </Card>
    )
}

export default NewTodoInput