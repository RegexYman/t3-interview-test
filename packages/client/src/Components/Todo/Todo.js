import { useState, useEffect } from 'react'
import { ListGroup, Button, Card } from 'react-bootstrap'
import axios from '../../axios/axios'
import NewTodoInput from './NewTodoInput/NewTodoInput'

const Todo = (props) => {
    const [todos, setTodos] = useState([])

    useEffect(async () => {
        let todos = await axios.getTodos()
        if (todos.success) {
            setTodos(todos.todo_lists)
        }
    }, [])

    const reloadTodoHandler = async () => {
        let todos = await axios.getTodos()
        if (todos.success) {
            setTodos(todos.todo_lists)
        }
    }

    return (
        <div>
            <h2>Todo</h2>
            <ListGroup>
                {
                    todos.length > 0 ?
                        todos.map((item) => {
                            return <ListGroup.Item action key={item.id} onClick={() => props.onTodoClicked(item.id, item.is_protected)}>{item.title}</ListGroup.Item>
                        })
                        :
                        <h5><center>Empty</center></h5>
                }
            </ListGroup>
            <br />
            <NewTodoInput
                reloadTodo={reloadTodoHandler}
            />
            <br />

        </div>
    )
}

export default Todo