import { useState } from 'react'
import { Table, Button, Form } from 'react-bootstrap'
import TodoListItem from './TodoListItem/TodoListItem'
import TodoListInput from './TodoListInput/TodoListInput'
import axios from '../../../axios/axios'

const TodoList = (props) => {
    let {
        id,
        title,
        items
    } = props.list

    let tempItems = items.sort((a, b) => {
        return new Date(a.due_date) - new Date(b.due_date)
    })

    const onDoneClickedHandler = async (item_id) => {
        try {
            if (props.protected) {
                let password = prompt("Todo is protected. Please enter password.")
                await axios.updateItemDone(id, item_id, password)
                await props.reloadTodoList(id, password)
            } else {
                await axios.updateItemDone(id, item_id, null)
                await props.reloadTodoList(id, null)
            }
        } catch (e) {
            if (typeof e.response.data !== "undefined") {
                alert(e.response.data.msg)
            }
        }
    }

    const onNewItemAddHandler = async (item_name, due_date, password) => {
        try {
            await axios.addTodoListItem(id, item_name, due_date, password)
            await props.reloadTodoList(id, password)
        } catch (e) {
            if (typeof e.response.data !== "undefined") {
                alert(e.response.data.msg)
            }
        }
    }

    const onDeleteItemClickHandler = async (item_id) => {
        try {
            if (props.protected) {
                let password = prompt("Todo is protected. Please enter password.")
                await axios.deleteItem(id, item_id, password)
                await props.reloadTodoList(id, password)
            }else{
                await axios.deleteItem(id, item_id, null)
                await props.reloadTodoList(id, null)
            }
        } catch (e) {
            if (typeof e.response.data !== "undefined") {
                alert(e.response.data.msg)
            }
        }
    }

    return (
        <div>
            <h2>
                <Button variant="primary" onClick={props.onBackClicked}>Back</Button>&nbsp;{title}
            </h2>
            <br />
            {
                tempItems.length > 0 ?
                    <Table hover size="md">
                        <tbody>
                            {
                                tempItems.map((item) => {
                                    return <TodoListItem
                                        key={item.id}
                                        item={item}
                                        onDoneClicked={onDoneClickedHandler}
                                        onDeleteClicked={onDeleteItemClickHandler}
                                    />
                                })
                            }
                        </tbody>
                    </Table>
                    :
                    <h5>
                        <center>Todo list is empty</center>
                    </h5>
            }
            <br />
            <TodoListInput
                protected={props.protected}
                onNewItemAdd={onNewItemAddHandler}
            />
        </div>
    )
}

export default TodoList