import { useState } from 'react'
import { ListGroup, Button, Card, Form } from 'react-bootstrap'
import axios from '../../../../axios/axios'

const TodoListInput = (props) => {
    const [itemName, setItemName] = useState('')
    const [dueDate, setDueDate] = useState('')

    const onItemNameChangeHandler = (event) => {
        setItemName(event.target.value)
    }

    const onDueDateChangeHandler = (event) => {
        setDueDate(event.target.value)
    }

    const onAddItem = async () => {
        let password = null
        if(props.protected){
            password = prompt('Please enter todo password.')
        }
        await props.onNewItemAdd(itemName,dueDate,password)
        setItemName('')
        setDueDate('')
    }

    return (
        <Card body>
            <h5>New Todo Item</h5>
            <Form>
                <Form.Group>
                    <Form.Label>Item name</Form.Label>
                    <Form.Control type="text" value={itemName} onChange={onItemNameChangeHandler} placeholder="Enter item name" />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Due date</Form.Label>
                    <Form.Control type='date' value={dueDate} onChange={onDueDateChangeHandler} min="2019-01-01" max="2049-12-31"  />
                </Form.Group>
            </Form>
            <br />
            <Button variant="success" onClick={onAddItem}>+ Add</Button>
        </Card>
    )
}

export default TodoListInput