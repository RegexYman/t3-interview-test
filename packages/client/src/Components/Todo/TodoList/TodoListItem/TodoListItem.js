import { useState } from 'react'
import { Form, Button } from 'react-bootstrap'

const TodoListItem = (props) => {
    let {
        id,
        description,
        due_date,
        done
    } = props.item

    return (
        <tr>
            <td><Form.Check type="checkbox" checked={done} onClick={() => props.onDoneClicked(id)} /></td>
            <td>{done ? <s>{description}</s> : description}</td>
            <td>Due date: {done ? <s>{due_date}</s> : due_date}</td>
            <td><Button variant="danger" onClick={() => props.onDeleteClicked(id)}>Delete</Button></td>
        </tr>
    )
}

export default TodoListItem