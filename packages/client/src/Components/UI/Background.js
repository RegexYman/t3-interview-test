import { useState } from 'react'
import { Containter } from 'react-bootstrap'

const Background = (props) => {
    return (
        <Containter>
            {props.children}
        </Containter>
    )
}

export default Background