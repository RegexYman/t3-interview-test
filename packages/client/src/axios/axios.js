import axios from 'axios'

const instance = axios.create({
    baseURL: 'http://localhost:3001/'
})

const getTodos = async () => {
    let todos = await instance.get('/getTodos')
    return todos.data
}

const getTodoList = async (id, password) => {
    let todoList = await instance.get(`/getTodoList/${id}`, {
        params: {
            password
        }
    })
    return todoList.data
}

const updateItemDone = async (todo_id, item_id, password) => {
    let doneReuslt = await instance.post(`/done/${todo_id}`, {
        id: item_id,
        password
    })
    return doneReuslt.data
}

const addTodo = async (title, password) => {
    let addResult = await instance.post('/addTodos', {
        title,
        password
    })
    return addResult.data
}

const addTodoListItem = async (todo_id, item_name, due_date, password) => {
    let addResult = await instance.post(`/addListItem/${todo_id}`, {
        description: item_name,
        due_date,
        password
    })
    return addResult.data
}

const deleteItem = async (todo_id, item_id, password) => {
    let deleteResult = await instance.post(`/removeListItem/${todo_id}`, {
        id: item_id,
        password
    })
    return deleteResult.data
}

export default {
    getTodos,
    getTodoList,
    updateItemDone,
    addTodo,
    addTodoListItem,
    deleteItem
}
